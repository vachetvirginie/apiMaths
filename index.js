const express = require('express');
let app = express();
const port = 8082;

//Routes//

//route add
app.get("/add", function(req, resp) {
    if (!req.query.a || !req.query.b) {
        resp.status(400);
        resp.send("missing parameter");
        return;
    }
    //recup param de l'url
    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
        resp.status(400);
        resp.send("invalid parameter");
        return;
    }
    resp.send("Le resultat est :" + (a + b).toString());

});
//route -
app.get("/sous", function(req, resp) {
    if (!req.query.a || !req.query.b) {
        resp.status(400);
        resp.send("missing parameter");
        return;
    }
    //recup param de l'url
    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
        resp.status(400);
        resp.send("invalid parameter");
        return;
    }
    resp.send("Le resultat est :" + (a - b).toString());
});

//route /
app.get("/div", function(req, resp) {
    if (!req.query.a || !req.query.b) {
        resp.status(400);
        resp.send("missing parameter");
        return;
    }
    //recup param de l'url
    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
        resp.status(400);
        resp.send("invalid parameter");
        return;
    }
    resp.send("Le resultat est :" + (a / b).toString());
});

//route x
app.get("/mul", function(req, resp) {
    if (!req.query.a || !req.query.b) {
        resp.status(400);
        resp.send("missing parameter");
        return;
    }
    //recup param de l'url
    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
        resp.status(400);
        resp.send("invalid parameter");
        return;
    }
    resp.send("Le resultat est :" + (a * b).toString());
});

//
//creation serveur
//
app.get('/', function(req, resp) {
    resp.sendfile(__dirname);
});

//on lance le serveur sur le port 8082
app.listen(8082);
//On affiche dans la console que le serveur tourne
console.log('Serveur lancé sur le port 8082');