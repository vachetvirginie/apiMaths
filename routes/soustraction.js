app.get("/sous", function(req, resp) {
    if (!req.query.a || !req.query.b) {
        resp.status(400);
        resp.send("missing parameter");
        return;
    }
    //recup param de l'url
    let a = parseFloat(req.query.a);
    let b = parseFloat(req.query.b);
    if (isNaN(a) || isNaN(b)) {
        resp.status(400);
        resp.send("invalid parameter");
        return;
    }
    resp.send((a - b).toString());
});