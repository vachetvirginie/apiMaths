# apiMaths

Math API

A simple API to do math calculation.
Addition

Realize an addition.

    URL:

    /add

    Method:

    GET

    URL Param:

    require:

    a=[number]

    b=[number]

    Success Response:

    Code: 200 OK

    Content: [number]

    Error Response:

    Code: 400 Bad Request

    Content: None

Substraction

Realize a substraction.

    URL:

    /sub

    Method:

    GET

    URL Param:

    require:

    a=[number]

    b=[number]

    Success Response:

    Code: 200 OK

    Content: [number]

    Error Response:

    Code: 400 Bad Request

    Content: None
